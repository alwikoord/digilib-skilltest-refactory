<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', ['judul' => 'Digilib.id | ']);
});
Route::get('contact', function () {
    return view('contact', ['judul' => 'Digilib.id | ']);
});

Route::get('novel', function () {
    return view('buku', ['judul' => 'Digilib.id | ']);
});



Route::get('form-kartu', function () {
    return view('form');
});

// Auth Security
Route::group(['middleware' => ['auth']], function(){

Route::get('dashboard', function () {
    return view('rangka');
});

//CRIUD Membership
Route::get('/membership/create', 'MemberhsipController@create');
Route::post('/membership', 'MemberhsipController@store');
Route::get('/membership', 'MemberhsipController@index');
Route::get('/membership/{membership_id}', 'MemberhsipController@show');
Route::get('/membership/{membership_id}/edit', 'MemberhsipController@edit');
Route::put('/membership/{membership_id}', 'MemberhsipController@update');
Route::delete('/membership/{membership_id}', 'MemberhsipController@destroy');

//CRIUD Kategori
Route::get('/kategori/create', 'KategorController@create');
Route::post('/kategori', 'KategorController@store');
Route::get('/kategori', 'KategorController@index');
Route::get('/kategori/{kategori_id}', 'KategorController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategorController@edit');
Route::put('/kategori/{kategori_id}', 'KategorController@update');
Route::delete('/kategori/{kategori_id}', 'KategorController@destroy');

//CRUD user admin
Route::get('/adminuse/create', 'AdminController@create');
Route::post('/adminuse', 'AdminController@store');
Route::get('/adminuse', 'AdminController@index');
Route::get('/adminuse/{admin_id}', 'AdminController@show');
Route::get('/adminuse/{admin_id}/edit', 'AdminController@edit');
Route::put('/adminuse/{admin_id}', 'AdminController@update');
Route::delete('/adminuse/{admin_id}', 'AdminController@destroy');

//CRUD Biblio
Route::resource('biblio', 'BiblioController');
Route::resource('bibliobook', 'BibliobookController');

});

//CRUD form kartu
Route::get('/form/form', 'FormController@create');
Route::post('/form', 'FormController@store');
Route::get('/form', 'FormController@index');

Route::get('/buku', 'BukuController@index');

Route::get('auth/google', [App\Http\Controllers\GoogleController::class, 'redirectToGoogle'])->name('google.login');
Route::get('auth/google/callback', [App\Http\Controllers\GoogleController::class, 'handleGoogleCallback'])->name('google.callback');


// Auth
Auth::routes();
