<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class membership extends Model
{
    protected $table = 'membership';

    protected $fillable = ['nama','nik','domisili','telepon','email','jk'];
}
