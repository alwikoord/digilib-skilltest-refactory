<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class biblio extends Model
{
    protected $table = 'biblio';
    protected $fillable = [
        'judul',
        'penulis',
        'penerbit',
        'tahun',
        'kategori_id'
    ];
}
