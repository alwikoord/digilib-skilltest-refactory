<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class MemberhsipController extends Controller
{
    public function create(){
        return view('membership.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'nik' => 'required',
            'domisili' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'jk' => 'required',
        ]);

        DB::table('membership')->insert([
            'nama' => $request['nama'],
            'nik' => $request['nik'],
            'domisili'  => $request['domisili'],
            'telepon' => $request['telepon'],
            'email' => $request['email'],
            'jk'  => $request['jk'],
        ]);

        Alert::success('Mantab', 'Data Berhasil Dimasukkan');
        return redirect('membership');
    }

    public function index(){
        $membership = DB::table('membership')->get();

        return view('membership.index', compact('membership'));
    }

    public function show($id){
        $membership = DB::table('membership')->where('id', $id)->first();

        Alert::info('Info Member', 'Detail Info Member');
        return view('membership.detail', compact('membership'));
    }

    public function edit($id){
        $membership = DB::table('membership')->where('id', $id)->first();
        return view('membership.edit', compact('membership'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'nik' => 'required',
            'domisili' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'jk' => 'required',
        ]);

        $query = DB::table('membership')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'nik' => $request['nik'],
                'domisili'  => $request['domisili'],
                'telepon' => $request['telepon'],
                'email' => $request['email'],
                'jk'  => $request['jk'],
                ]);

         Alert::success('Mantab', 'Data Berhasil Diupdate');
        return redirect('/membership');
    }

    public function destroy($id){
        DB::table('membership')->where('id', $id)->delete();

        Alert::success('Mantab', 'Data Berhasil Dihapus');
        return redirect('/membership');
    }
}