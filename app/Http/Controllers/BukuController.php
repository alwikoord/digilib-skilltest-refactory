<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Bibliobook;
use File;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bibliobook = Bibliobook::all();
        return view('buku', compact('bibliobook'));
    }
}
