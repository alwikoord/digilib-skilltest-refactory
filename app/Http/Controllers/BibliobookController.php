<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Bibliobook;
use File;

class BibliobookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bibliobook = Bibliobook::all();
        return view('bibliobook.index', compact('bibliobook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategor')->get();
        return view('bibliobook.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'kategori_id' => 'required',
            'sampul' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $SampulName = time().'.'.$request->sampul->extension(); 
        $request->sampul->move(public_path('sampul'), $SampulName);

        $bibliobook = new Bibliobook;

        $bibliobook->judul = $request->judul;
        $bibliobook->penulis = $request->penulis;
        $bibliobook->penerbit = $request->penerbit;
        $bibliobook->tahun = $request->tahun;
        $bibliobook->kategori_id = $request->kategori_id;
        $bibliobook->sampul = $SampulName;

        $bibliobook->save();

        return redirect('/bibliobook');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bibliobook = Bibliobook::findOrfail($id);
        return view('bibliobook.detail', compact('bibliobook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategor')->get();
        $bibliobook = Bibliobook::findOrfail($id);

        return view('bibliobook.edit', compact('bibliobook', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'kategori_id' => 'required',
            'sampul' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $bibliobook = Bibliobook::find($id);

        if($request->has('sampul')){

            $SampulName = time().'.'.$request->sampul->extension(); 
            $request->sampul->move(public_path('sampul'), $SampulName);

            $bibliobook->judul = $request->judul;
            $bibliobook->penulis = $request->penulis;
            $bibliobook->penerbit = $request->penerbit;
            $bibliobook->tahun = $request->tahun;
            $bibliobook->kategori_id = $request->kategori_id;
            $bibliobook->sampul = $SampulName;

        }else{
            $bibliobook->judul = $request->judul;
            $bibliobook->penulis = $request->penulis;
            $bibliobook->penerbit = $request->penerbit;
            $bibliobook->tahun = $request->tahun;
            $bibliobook->kategori_id = $request->kategori_id;
        }

        $bibliobook->update();

        return redirect('/bibliobook');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bibliobook = Bibliobook::find($id);
        $path = "sampul/";
        File::delete($path.$bibliobook->sampul);
        $bibliobook->delete();

        return redirect('/bibliobook');
        
    }
}
