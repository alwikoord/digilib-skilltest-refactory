<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class KategorController extends Controller
{
    public function create(){
        return view('kategori.create');
    }
    public function store(Request $request){
        $request->validate([
            'kategori' => 'required',
        ]);

        DB::table('kategor')->insert([
            'kategori' => $request['kategori'],
        ]);

        Alert::success('Mantab', 'Data Berhasil Dimasukkan');
        return redirect('kategori');
    }

    public function index(){
        $kategori = DB::table('kategor')->get();

        return view('kategori.index', compact('kategori'));
    }

    public function show($id){
        $kategori = DB::table('kategor')->where('id', $id)->first();
        return view('kategori.detail', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategor')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'kategori' => 'required',
        ]);

        $query = DB::table('kategor')
              ->where('id', $id)
              ->update([
                'kategori' => $request['kategori'],
                ]);
        Alert::success('Mantab', 'Data Berhasil Diupdate');
        return redirect('/kategori');
    }

    public function destroy($id){
        DB::table('kategor')->where('id', $id)->delete();

        Alert::success('Mantab', 'Data Berhasil Dihapus');
        return redirect('/kategori');
    }
}
