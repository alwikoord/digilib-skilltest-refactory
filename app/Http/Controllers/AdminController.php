<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function create(){
        return view('adminuse.create');
    }
    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        DB::table('users')->insert([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        Alert::success('Mantab', 'Data Berhasil Dimasukkan');
        return redirect('adminuse');
    }

    public function index(){

        $users = DB::table('users')->get();
        return view('adminuse.index', compact('users'));
    }

    public function show($id){
        $users = DB::table('users')->where('id', $id)->first();
        return view('adminuse.detail', compact('users'));
    }

    public function edit($id){
        $users = DB::table('users')->where('id', $id)->first();
        return view('adminuse.edit', compact('users'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            
        ]);

        $query = DB::table('users')
              ->where('id', $id)
              ->update([
                'name' => $request['name'],
                'email' => $request['email'],
                ]);
        
         Alert::success('Mantab', 'Data Berhasil Diupdate');
        return redirect('/adminuse');
    }

    public function destroy($id){
        DB::table('users')->where('id', $id)->delete();

        Alert::success('Mantab', 'Data Berhasil Dihapus');
        return redirect('/adminuse');
    }
}
