<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bibliobook extends Model
{
    protected $table = 'bibliobook';
    protected $fillable = [
        'judul',
        'penulis',
        'penerbit',
        'tahun',
        'kategori_id',
        'sampul'
    ];
}
