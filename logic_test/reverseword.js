const kalimat = "I Am A Great Human ";
        const reversedWholeSentence = reversedBySeparator(kalimat, "");
        const reversedEachSentence = reversedBySeparator(reversedWholeSentence, " ");
        console.log(reversedEachSentence);

        function reversedBySeparator(string, separator) {
            return string.split(separator).reverse().join(separator);
        }