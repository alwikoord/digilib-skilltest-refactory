function checkpalindrome(set) {
    let strArray = set.toLowerCase().split('');
    let newArr = strArray.join('');
    let reverseArr = [...newArr].reverse().join('');
    console.log(newArr)

    if ( newArr === reverseArr) {
      return "~~~~ Is Palindrome";
    } else {
      return "~~~~ not Palindrome";
    }
    
};

console.log(checkpalindrome("Radar"));
console.log(checkpalindrome("Malam"));
console.log(checkpalindrome("Kasur ini rusak"));
console.log(checkpalindrome("Ibu Ratna antar ubi"));
console.log(checkpalindrome("Malas"));
console.log(checkpalindrome("Makan nasi goreng"));
console.log(checkpalindrome("Balonku ada lima"));