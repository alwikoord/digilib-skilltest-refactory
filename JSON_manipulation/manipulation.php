<?php

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://localhost/skilltest_refactory/JSON_manipulation/data.json");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $output = curl_exec($ch);
    curl_close($ch);

    $data = json_decode($output);
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>JSON - manipulation</title>
 </head>
 <body>
     <?php
        foreach($data as $barang){
            echo'
                <label>Inventory id : </label>
                '.$barang->inventory_id.'  <br/>
                
                <label>Nama Barang : </label>
                '.$barang->name.'  <br/>

                <label>Type : </label>
                '.$barang->type.'  <br/>

                <label>Purchased at : </label>
                '.$barang->purchased_at.'  <br/>
            ';

        echo "<label>Tags :</label><br/>";   
        foreach($barang->tags as $tags){
            echo "- ".$tags;
            echo "<br/>";
        }
        echo "<hr/>";
        }

     ?>
 </body>
 </html>