@extends('main')

@section('judul', 'Admin')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Create Data</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/adminuse" class="btn btn-danger btn-bg mb-3" >Back</a>

<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1"><b>Create Admin User | </b>Digital Library Indonesia</a>
    </div>
    <div class="card-body">
      <form action="/adminuse" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Full name">
        </div>

        @error('name')
         <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
        </div>

        @error('email')
         <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
        </div>

        @error('password')
         <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
        </div>

        <div class="row">
            <button type="submit" class="btn btn-primary btn-block">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

