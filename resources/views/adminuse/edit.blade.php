@extends('main')

@section('judul', 'Admin')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Edit User Admin</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/adminuse" class="btn btn-danger btn-bg mb-3" >Back</a>

<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1"><b>Edit Admin User | </b>Digital Library Indonesia</a>
    </div>
    <div class="card-body">
      <form action="/adminuse/{{$users->id}}" method="post">
        @csrf
        @method('PUT')

        <div class="input-group mb-3">
          <input type="text" name="name" value="{{$users->name}}"class="form-control" placeholder="Full name">
        </div>

        @error('name')
         <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
            <input type="text" name="email" value="{{$users->email}}"class="form-control" placeholder="Email">
          </div>
  
          @error('email')
           <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        <div class="row">
            <button type="submit" class="btn btn-primary btn-block">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

