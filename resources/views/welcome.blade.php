@extends('landing')

@section('judul', 'Welcome')
    
@section('cons')
<div class="page-content">

  <div class="home-hero" data-src="{{asset("style/images/coba1.png")}}" uk-img>
      <div class="uk-width-4-1">
          <div class="page-content-inner uk-position-z-index">
              <h1 style="color: #5d0505;">Welcome to <br>Digital Library Indonesia</h1>
              <h3 class="my-lg-3" style="color: #2c2904;"> ~~ Solusi Buku Digital Masa Kini ~~</h3>

          </div>
      </div>
  </div>

  <div class="section">
      <div class="page-content-inner">

          <div class="section-small text-md-left text-center">
              <div class="uk-child-width-1-2@m uk-gird-large uk-flex-middle" uk-grid>
                  <div>
                      <img src="{{asset('style/images/logo_digi.png')}}" alt="" width="400px">
                  </div>
                  <div>
                      <h2>Digital Library Indonesia </h1>
                          <p> Digital Library Indonesia merupakan platform penyedia buku-buku dan novel 
                              online, dimana platform ini dibentuk dan dibuat berdasarkan tingkat digitalisasi
                              di Indonesia semakin meningkat dalam berbagai bidang.
                          </p>
                          <p> Digital Library Indonesia didirikan oleh seorang mahasiswa yang ingin membaca 
                              buku saat pandemi, diaman semua perpustakaan tidak bisa dikunjungi secara offline.
                              Digital Library Indonesia ini didirikan pada tanggal 10 Agutus 2021, di Ponorogo - Indonesia.
                          </p>
                          <a href="#" class="btn btn-soft-light">Selengkapnya </a>
                  </div>
              </div>
          </div>

      </div>
  </div>

  <div class="section-small delimiter-top">

      <div class="container-small">

          <div class="text-center mb-5">
              <h3> Layanan </h3>
              <h5> Digital Library Indonesia sebagai platform penyedia Buku dan novel secara online dengan
                   layanan yang dapat dipilih sesuai kebutuhan Anda.</h5>
          </div>
          <div class="course-grid-slider mt-lg-5" uk-slider="finite: true">
              <div class="uk-slider-container pb-3">
                  <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m uk-grid">
                      <li>
                          <a href="software.html">
                              <div class="course-card">
                                  <div class="course-card-thumbnail ">
                                      <img src="{{asset('style/images/course/ln1.png')}}">

                                  </div>
                                  <div class="course-card-body">
                                      <div class="course-card-info">


                                      </div>
                                      <h4>Novel Collection </h4>
                                      <p> Penyedia novel-novel secara online di masa ini. </p>

                                  </div>

                              </div>
                          </a>

                      </li>
                      <li>
                          <a href="mager.html">
                              <div class="course-card">
                                  <div class="course-card-thumbnail ">
                                      <img src="{{asset("style/images/course/ln2.png")}}">

                                  </div>
                                  <div class="course-card-body">
                                      <div class="course-card-info">
                                      </div>
                                      <h4>Digilib e-card </h4>
                                      <p> Penyedia jasa pembuatan e-card dilib.id.
                                      </p>
                                  </div>

                              </div>
                          </a>

                      </li>
                      <li>
                          <a href="premium.html">
                              <div class="course-card">
                                  <div class="course-card-thumbnail ">
                                      <img src="{{asset('style/images/course/ln3.png')}}">

                                  </div>
                                  <div class="course-card-body">
                                      <div class="course-card-info">


                                      </div>
                                      <h4>Book Collection </h4>
                                      <p> Penyedia buku-buku secara online di era ini. </p>

                                  </div>

                              </div>
                          </a>

                      </li>
                      
                  </ul>
                  
                  <a class="uk-position-center-left uk-position-small uk-hidden-hover slidenav-prev" href="#"
                      uk-slider-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover slidenav-next" href="#"
                      uk-slider-item="next"></a>

              </div>
          </div>

          <div class="text-center">
              <a href="#" class="btn btn-soft-light btn-small btn-circle"> Selanjutnya</a>
          </div>
      </div>
  </div>
@endsection

  





  


