@extends('main')

@section('judul', 'Edit | Kategori')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Edit Kategori Biblio</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/kategori" class="btn btn-danger btn-bg mb-3" >Back</a>
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Kategori</label>
      <input type="text" name="kategori" value="{{$kategori->kategori}}" class="form-control">
    </div>
    @error('kategori')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-info">Submit</button>
  </form>

  
@endsection