@extends('main')

@section('judul', 'Front | Kategori')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Kategori</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="kategori/create" class="btn btn-success btn-bg mb-3" >Create Data</a>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Kategori Biblio</th>
        <th scope="col">Action</th>
      </tr>
      
      @foreach ($kategori as $item)
      <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->kategori }}</td>
        <td>        
            <form action="/kategori/{{$item->id}}" method="POST">
                @method('delete')
                @csrf
                <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
      @endforeach        
    </thead>
</table>


  
@endsection