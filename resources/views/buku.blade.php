@extends('landing')

@section('judul', 'Koleksi Buku')
    
@section('cons')

<div class="page-content pt-sm-0">

    <div class="page-content-inner p-sm-0"> 


    <div uk-grid>
        
        <div class="uk-width-expand@m m-4 mt-0">
            <div class="my-4">
                <h2 class="uk-text-bold">Book Collection</h2>
                <hr class="m-0">
            </div>
            <div class="row">
              @forelse ($bibliobook as $item)
              <div class="col-4">
                  <div class="card" style="width: 30rem;">
                      <img src="{{asset('sampul/'. $item->sampul)}}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h2 class="card-title" style="text-align: center">{{$item->judul}}</h2>
                        <h3 class="card-title" style="text-align: center">Tahun : {{$item->tahun}}</h3> <br>
                        <div class="card-footer">
                          <a href="login" class="btn btn-info btn-lg btn-block">Read More ..</a>
                        </div>
                      </div>
                    </div>
              </div>
              @empty
                  <h3>Data Biblio Kosong ....</h3>
              @endforelse
              
            </div>

        </div>

    </div>
    
  
  </div>
@endsection