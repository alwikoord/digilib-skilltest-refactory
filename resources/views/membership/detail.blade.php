@extends('main')

@section('judul', 'Detail | Membership')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Detail e-librarian card</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/membership" class="btn btn-danger btn-bg mb-3" >Back</a>
<h3>Nama Lengkap : {{$membership->nama}}</h3>
<h4>Nik : {{$membership->nik}} </h4>
<h4>Domisili : {{$membership->domisili}} </h4>
<h4>Telepon : {{$membership->telepon}} </h4>
<h4>Email : {{$membership->email}} </h4>
<h4>Jenis Kelamin : {{$membership->jk}} </h4>
  
@endsection