@extends('main')

@section('judul', 'Edit | Membership')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Edit e-librarian card</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/membership" class="btn btn-danger btn-bg mb-3" >Back</a>
<form action="/membership/{{$membership->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$membership->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>NIK</label>
      <input type="number" name="nik" value="{{$membership->nik}}" class="form-control">
    </div>
    @error('nik')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Domisili</label>
      <input type="text" name="domisili" value="{{$membership->domisili}}" class="form-control">
    </div>
    @error('domisili')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Telepon</label>
      <input type="number" name="telepon" value="{{$membership->telepon}}" class="form-control">
    </div>
    @error('telepon')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" value="{{$membership->email}}" class="form-control">
    </div>
    @error('email')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Jenis Kelamin</label>
      <select class="form-control" name="jk" value="{{$membership->jk}}">
             <option>Laki-Laki</option>
             <option>Perempuan</option>
            </select>
    </div>
    @error('jk')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-info">Submit</button>
  </form>

  
@endsection