@extends('main')

@section('judul', 'Front | Membership')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Membership</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="membership/create" class="btn btn-success btn-bg mb-3" >Create Data</a>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Nim / Nik</th>
        <th scope="col">Domisili</th>
        <th scope="col">Telepon</th>
        <th scope="col">JK</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
      </tr>
      
      @foreach ($membership as $item)
      <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->nik }}</td>
        <td>{{ $item->domisili }}</td>
        <td>{{ $item->telepon }}</td>
        <td>{{ $item->jk }}</td>
        <td>{{ $item->email }}</td>
        <td>        
            <form action="/membership/{{$item->id}}" method="POST">
                @method('delete')
                @csrf
                <a href="/membership/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/membership/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
      @endforeach        
    </thead>
</table>


  
@endsection