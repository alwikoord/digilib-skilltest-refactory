<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        Login | Digilib.id
    </title>
    <link href="{{asset('components/core/img/favicon.png')}}" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i,900" rel="stylesheet">

    <!-- VENDORS -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/font-feathericons/dist/feather.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/font-linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/font-icomoon/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/chart.js/dist/Chart.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/jqvmap/dist/jqvmap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/c3/c3.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/bs4/dt-1.10.18/fc-3.2.5/r-2.2.2/datatables.min.css" />
    <link rel="stylesheet" type="text/css"
        href="/assets/vendors/tempus-dominus-bs4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/owl.carousel/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/ionrangeslider/css/ion.rangeSlider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/bootstrap-sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/nprogress/nprogress.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/summernote/dist/summernote.css"')}}>
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/dropify/dist/css/dropify.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/jquery-steps/demo/css/jquery.steps.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}">


    <script src="{{asset('/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('/vendor/popper.js/dist/umd/popper.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{asset('/vendor/jquery-mousewheel/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('/vendor/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('/vendor/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{asset('/vendor/jqvmap/dist/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('/vendor/jqvmap/dist/maps/jquery.vmap.usa.js')}}"></script>
    <script src="{{asset('/vendor/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('/vendor/d3/d3.min.js')}}"></script>
    <script src="{{asset('/vendor/d3-dsv/dist/d3-dsv.js')}}"></script>
    <script src="{{asset('/vendor/d3-time-format/dist/d3-time-format.js')}}"></script>
    <script src="{{asset('/vendor/c3/c3.min.js')}}"></script>
    <script src="{{asset('/vendor/peity/jquery.peity.min.js')}}"></script>
    <script type="text/javascript"
        src="/https://cdn.datatables.net/v/bs4/dt-1.10.18/fc-3.2.5/r-2.2.2/datatables.min.js"></script>
    <script src="{{asset('/vendor/editable-table/mindmup-editabletable.js')}}"></script>
    <script src="{{asset('/vendor/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('/vendor/tempus-dominus-bs4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <script src="{{asset('/vendor/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('/vendor/owl.carousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/vendor/ionrangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{asset('/vendor/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap-sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('/vendor/nprogress/nprogress.js')}}"></script>
    <script src="{{asset('/vendor/summernote/dist/summernote.min.js')}}"></script>
    <script src="{{asset('/vendor/nestable/jquery.nestable.js')}}"></script>
    <script src="{{asset('/vendor/jquery-typeahead/dist/jquery.typeahead.min.js')}}"></script>
    <script src="{{asset('/vendor/autosize/dist/autosize.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap-show-password/dist/bootstrap-show-password.min.js')}}"></script>
    <script src="{{asset('/vendor/dropify/dist/js/dropify.min.js')}}"></script>
    <script src="{{asset('/vendor/html5-form-validation/dist/jquery.validation.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{asset('/vendor/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
    <script src="{{asset('/vendor/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>

    <!-- AIR UI HTML ADMIN TEMPLATE MODULES-->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="{{asset('components/vendors/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/core/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/widgets/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/system/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/menu-left/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/menu-top/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/footer/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/topbar/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/subbar/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/sidebar/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/chat/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/apps/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/apps/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/extra-apps/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('components/ecommerce/style.css')}}">

    <script src="{{asset('/components/core/index.js')}}"></script>
    <script src="{{asset('/components/menu-left/index.js')}}"></script>
    <script src="{{asset('/components/menu-top/index.js')}}"></script>
    <script src="{{asset('/components/sidebar/index.js')}}"></script>
    <script src="{{asset('/components/topbar/index.js')}}"></script>
    <script src="{{asset('/components/chat/index.js')}}"></script>

    <!-- PRELOADER STYLES-->
    <style>
        .air__initialLoading {
            position: fixed;
            z-index: 99999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-position: center center;
            background-repeat: no-repeat;
            background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDFweCIgIGhlaWdodD0iNDFweCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDEwMCAxMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIiBjbGFzcz0ibGRzLXJvbGxpbmciPiAgICA8Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiBmaWxsPSJub25lIiBuZy1hdHRyLXN0cm9rZT0ie3tjb25maWcuY29sb3J9fSIgbmctYXR0ci1zdHJva2Utd2lkdGg9Int7Y29uZmlnLndpZHRofX0iIG5nLWF0dHItcj0ie3tjb25maWcucmFkaXVzfX0iIG5nLWF0dHItc3Ryb2tlLWRhc2hhcnJheT0ie3tjb25maWcuZGFzaGFycmF5fX0iIHN0cm9rZT0iIzAxOTBmZSIgc3Ryb2tlLXdpZHRoPSIxMCIgcj0iMzUiIHN0cm9rZS1kYXNoYXJyYXk9IjE2NC45MzM2MTQzMTM0NjQxNSA1Ni45Nzc4NzE0Mzc4MjEzOCIgdHJhbnNmb3JtPSJyb3RhdGUoODQgNTAgNTApIj4gICAgICA8YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgY2FsY01vZGU9ImxpbmVhciIgdmFsdWVzPSIwIDUwIDUwOzM2MCA1MCA1MCIga2V5VGltZXM9IjA7MSIgZHVyPSIxcyIgYmVnaW49IjBzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSI+PC9hbmltYXRlVHJhbnNmb3JtPiAgICA8L2NpcmNsZT4gIDwvc3ZnPg==);
            background-color: #fff;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.air__initialLoading').delay(200).fadeOut(400)
        })
    </script>
</head>

<body class="air__menu--gray">
    <div class="air__initialLoading"></div>
    <div class="air__layout__content">
        <div class="air__utils__content">
            <div class="air__auth">
                <div class="pt-5 pb-5 d-flex align-items-end mt-auto">
                    <img src="#" alt="" width="100px"/>
                </div>
                <div class="air__auth__container pl-5 pr-5 pt-5 pb-5 bg-white text-center">
                    <img src="assets/images/logo_digi.png" alt="" width="100px"/>
                    <div class="text-dark font-size-30 mb-4"><strong>Admin | </strong>Area</div>
                    
                    <form action="{{ route('login') }}" method="post">
                      @csrf
                      <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                        @error('email')
                           <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fa fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control @error('email') is-invalid @enderror" placeholder="Password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fa fa-lock"></span>
                          </div>
                        </div>
                      </div>
              
                      @error('password')
                       <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                      
                      <a href="{{ route('google.login') }}"
                        class="air__auth__googleSign font-weight-bold font-size-18 text-dark btn btn-outline-light w-100 mb-3">Log
                        in with Google</a>
                      <button type="submit" class="text-center btn btn-success w-100 font-weight-bold font-size-18">
                        Log In
                    </button>

                    </form><br>
                    <a href="/" class="text-blue font-weight-bold font-size-18">Forgot password?</a>
                    <br>Don't have an account?
                    <a href="register" class="font-weight-bold text-blue text-underlined"><u>Sign Up</u></a>
                </div>
                <div class="mt-auto pb-5 pt-5">
                    <p  style="text-align: center"><strong>Follow us</strong></p>
                    <ul class="air__auth__footerNav list-unstyled d-flex mb-2 flex-wrap justify-content-center">
                        <li><a class="social-media-icon" href="#"><span class="fa fa-twitter"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-facebook"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-instagram"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-linkedin"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-youtube"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-skype"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-tumblr"></span></a></li>
                        <li><a class="social-media-icon" href="#"><span class="fa fa-whatsapp"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
