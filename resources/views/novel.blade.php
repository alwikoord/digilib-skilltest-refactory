@extends('landing')

@section('judul', 'Koleksi Novel')
    
@section('cons')

<div class="page-content pt-sm-0">

    <div class="page-content-inner p-sm-0"> 


    <div uk-grid>
        
        <div class="uk-width-expand@m m-4 mt-0">
            <div class="my-4">
                <h2 class="uk-text-bold">Novel Collection</h2>
                <hr class="m-0">
            </div>
            <div class="card-deck">
                <div class="card">
                  <img src="assets/images/course/un1.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Iflix VIP</h5>
                    <p class="card-text">Nikmatin keseruan menonton film sambil ngopi tanpa iklan dan gangguan apapun, yuk mari di order..</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                  <img src="assets/images/course/un2.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Netflix Private</h5>
                    <p class="card-text">Kumpulan film yang luar biasa, yuk mari nonton dengan Netflix private sambil nugas tanpa lelet dan iklan, yuk gasskeun order ..</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                  <img src="assets/images/course/un3.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">WeTV Original VIP</h5>
                    <p class="card-text">Platform film-film series dari Indonesia, jepang, dan negara lainnya, nonton film tanpa diblok karena belum VIP yuk di order...</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                    <img src="assets/images/course/un4.png" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Viu Premium</h5>
                      <p class="card-text">nikmatin keseruan menonton Drama Korea tanpa gangguan sedikitpun, rugi kalo gak order, yuk mari order....</p>
                    </div>
                    <div class="card-footer">
                      <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                    </div>
                  </div>
              </div> <br>
              <div class="card-deck">
                <div class="card">
                  <img src="assets/images/course/un5.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Sportify Premium</h5>
                    <p class="card-text">Mari simpan dan download playlist lagumu dengan bebas dan dengarkan secara offline, tanpa basa-basi hayuk mah ya di Order...</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                  <img src="assets/images/course/un6.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Vidio Platinum</h5>
                    <p class="card-text">Platform televisi nasional bahkan internasional dan didalamnya juga terdapat kumpulan film-film yang gak kalah menarik, yuk mari di Order...</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                  <img src="assets/images/course/un7.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Adobe Lightroom Premium</h5>
                    <p class="card-text">Mari ngedit foto biar keren kaya horang-horang dengan adobe lightroom premium, yuk mari di order gan.....</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                    <img src="assets/images/course/un8.png" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Zoom Premium</h5>
                      <p class="card-text">Nikmatin keseruan kumpul online tanpa batas waktu dan miliki banyak fitur seperti record, co-host dan Unlimited time, yuk mari-mari merapat...</p>
                    </div>
                    <div class="card-footer">
                      <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                    </div>
                  </div>
              </div> <br>
              <div class="card-deck">
                <div class="card">
                  <img src="assets/images/course/un9.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Google Drive Unlimited</h5>
                    <p class="card-text">Nikmati penyimpanan cloud tanpa batas dengan memilki google drive unlimited selamanya, mari order....</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                <div class="card">
                  <img src="assets/images/course/un10.png" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Youtube Premium</h5>
                    <p class="card-text">Nonton vidio dan musik tanpa iklan..? pake youtube premium aja, dengan langganan premium pastinya tidak ada iklan, yuk mari....</p>
                  </div>
                  <div class="card-footer">
                    <a href="#" class="btn btn-success btn-lg btn-block">O R D E R</a>
                  </div>
                </div>
                
              </div>

        </div>


    </div>

@endsection