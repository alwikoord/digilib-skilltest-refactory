@extends('landing')

@section('judul', 'Contact')
    
@section('cons')
<div class="page-content">

    <div class="page-content-inner">


        <!-- Intro banner container  -->
        <h1 class="mb-0">Mari Bincang !!!</h1>
        <p class="mt-2">Please contact us :</p>
        
        <a href="#"><i class="icon-brand-whatsapp"> 0812-5348-5723</i></a><br>
        <a href="#"><i class="fa fa-envelope"> digilib@gmail.com</i></a><br>             
    
        <div class="uk-child-width-1-2@m uk-flex-middle" uk-grid>

            <div>

                <p class="mt-2">or send massage to :</p>
                <form uk-grid="" class="uk-grid">
                    <div class="uk-width-1-2@s uk-first-column">
                        <label class="uk-form-label">Name</label>
                        <input class="uk-input" type="text" placeholder="Name">
                    </div>
                    <div class="uk-width-1-2@s">
                        <label class="uk-form-label">Email</label>
                        <input class="uk-input" type="text" placeholder="Email">
                    </div>
                    <div class="uk-width-1-1@s uk-grid-margin uk-first-column">
                        <label class="uk-form-label">Message</label>
                        <textarea class="uk-textarea" placeholder="Enter Your Message her..."
                            style=" height:160px"></textarea>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <input type="submit" value="Send" class="button success">
                    </div>
                </form>

            </div>
            <div>

                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.229739006255!2d111.46029931451874!3d-7.871012280442892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e799f854b1d79bb%3A0xae2d8ff527ad1f01!2sAlun-Alun%20Ponorogo!5e0!3m2!1sid!2sid!4v1637764277081!5m2!1sid!2sid"
                    width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

            </div>

        </div>


    </div>
    
@endsection