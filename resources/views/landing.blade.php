<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('judul') - Digilib Indonesia</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="Courseplus - Professional Learning Management HTML Template">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/night-mode.css')}}">
    <link rel="stylesheet" href="{{asset('css/framework.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

    <!-- icons
    ================================================== -->
    <link rel="stylesheet" href="{{asset('css/icons.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<!-- Wrapper -->
<div id="wrapper" class="bg-white">

  <!-- Header Container
  ================================================== -->
  <header class="header header-horizontal">

      <div class="container">
          <nav uk-navbar>

              <!-- left Side Content -->
              <div class="uk-navbar-left">

                  <!-- menu icon -->
                  <span class="mmenu-trigger">
                      <button class="hamburger hamburger--collapse" type="button">
                          <span class="hamburger-box">
                              <span class="hamburger-inner"></span>
                          </span>
                      </button>
                  </span>

                  <!-- logo -->
                  <a href="#" class="logo">
                      <img src="{{asset('style/images/loghk.png')}}" alt="" width="400px">
                  </a>

                  <!-- Main Navigation -->

                  <nav id="navigation">
                      <ul id="responsive">
                          <li><a href="/" class="nav-active">Digital Library Indonesia | </a> </li>
                          <li><a href="#">Layanan</a>
                              <ul class="dropdown-nav nav-large nav-courses">

                                  
                                  <li><a href="/form">
                                          <i class="icon-brand-google-play" style="color: #f7df1e;"></i> Digilib Card  </a>
                                  </li>
                                  <li><a href="/buku">
                                          <i class="icon-feather-book" style="color:#0a7bc7"></i> Koleksi Buku </a>
                                  </li>
                                  <li><a href="#">
                                          <i class="icon-feather-shopping-bag" style="color:#df0f0f"></i> Koleksi Novel </a></li>
                                  
                              </ul>
                               <li><a href="/contact">Contact</a> </li>
                      </ul>
                  </nav>
                  <!-- Main Navigation / End -->

              </div>


              <!--  Right Side Content   -->

              <div class="uk-navbar-right">

                  <div class="header-widget">

                      <div class="searchbox uk-visible@s">

                          <input class="uk-search-input" type="search" placeholder="Search...">
                          <button class="btn-searchbox"> </button>

                      </div>
                      <!-- Search box dropdown -->
                      <div uk-dropdown="pos: top;mode:click;animation: uk-animation-slide-bottom-small"
                          class="dropdown-search">
                          <div class="erh BR9 MIw"
                              style="top: -27px;position: absolute ; left: 24px;fill: currentColor;height: 24px;pointer-events: none;color: #f5f5f5;">
                              <svg width="22" height="22">
                                  <path d="M0 24 L12 12 L24 24"></path>
                              </svg></div>
                          <!-- User menu -->

                          <ul class="dropdown-search-list">
                              <li class="list-title">
                                  Recent Searches
                              </li>
                              <li>
                                  <a href="#">
                                      Power Point Editing</a>
                              </li>
                              <li><a href="#">
                                      Iflix Premium </a>
                              </li>
                      </div>

                      <!-- notification contents -->



                      <!-- profile-icon-->

                      <a href="#" class="header-widget-icon profile-icon">
                          <img src="{{asset('style/images/set.png')}}" alt="" class="header-profile-icon">
                      </a>

                      <div uk-dropdown="pos: top-right ;mode:click" class="dropdown-notifications small">

                          <div class="dropdown-user-details">
                              <div class="dropdown-user-name">
                                  Setting Area
                              </div>
                          </div>

                          <ul class="dropdown-user-menu">
                              <li>
                                  <a href="#" id="night-mode" class="btn-night-mode">
                                      <i class="icon-feather-moon"></i> Night mode
                                      <span class="btn-night-mode-switch">
                                          <span class="uk-switch-button"></span>
                                      </span>
                                  </a>
                              </li>
                              <li class="menu-divider">
                              <li><a href="register">
                                  <i class="icon-brand-uniregistry"></i> Register</a>
                              </li>
                              <li><a href="/login">
                                      <i class="icon-feather-log-out"></i> Log In</a>
                              </li>
                          </ul>
                      </div>
                  </div>



                  <!-- icon search-->
                  <a class="uk-navbar-toggle uk-hidden@s"
                      uk-toggle="target: .nav-overlay; animation: uk-animation-fade" href="#">
                      <i class="uil-search icon-small"></i>
                  </a>

                  <!-- User icons -->
                  <a href="#" class="uil-user icon-small uk-hidden@s"
                      uk-toggle="target: .header-widget ; cls: is-active">
                  </a>

              </div>
              <!-- End Right Side Content / End -->


          </nav>

      </div>
      <!-- container  / End -->

  </header>

  <div class="nav-overlay uk-navbar-left uk-position-relative uk-flex-1 bg-grey uk-light p-2" hidden
      style="z-index: 10000;">
      <div class="uk-navbar-item uk-width-expand" style="min-height: 60px;">
          <form class="uk-search uk-search-navbar uk-width-1-1">
              <input class="uk-search-input" type="search" placeholder="Search..." autofocus>
          </form>
      </div>
      <a class="uk-navbar-toggle" uk-close uk-toggle="target: .nav-overlay; animation: uk-animation-fade"
          href="#"></a>
  </div>


  @yield('cons')
  

  </div>

  <!-- footer
  ================================================== -->
  <div class="footer">
      <div class="container">
          <div uk-grid>
              <div class="uk-width-1-2@m">
                  <a href="home.html" class="uk-logo">
                      <!-- logo icon -->
                      <i class="uil-graduation-hat"> </i>
                      Digital Library Indonesia
                  </a>
                  <p class="footer-description"> Kawasan Kota Pemerintahan,
                      Siman, Ponorogo, Jawa Timur.</p>
                  <p>digilib@gmail.com</p>
              </div>
              <div class="uk-width-expand@s uk-width-1-2">
                  <div class="footer-links pl-lg-8">
                      <!-- <h5>Kontak Kami </h5>
                      <ul>
                          <li><a href="course-card.html">  +62 812 1561 131 (Eko) </a></li>
                      </ul> -->

                  </div>
              </div>
              <div class="uk-width-expand@s uk-width-1-2">
                  <div class="footer-links pl-lg-8">
                      <!-- <h5> Account </h5>
                      <ul>
                          <li><a href="profile-1.html"> About Us </a></li>
                          <li><a href="#"> Product Catalog </a></li>
                          
                      </ul> -->
                  </div>
              </div>
              <div class="uk-width-expand@s uk-width-1-2">
                  <div class="footer-links pl-lg-8">
                      <img src="{{asset('style/images/logo_digi.png')}}" alt="" width="130px">
                  </div>
              </div>
          </div>
          <hr>
          <div class="uk-grid-collapse" uk-grid>
              <div class="uk-width-expand@s uk-first-column">
                  <p>© 2021 <strong>Digilib.id</strong></p>
              </div>
              <div class="uk-width-auto@s">
                  <nav class="footer-nav-icon">
                      <ul>
                          <li><a href="#"><i class="icon-brand-facebook"></i></a></li>
                          <li><a href="#"><i class="icon-brand-instagram"></i></a></li>
                          <li><a href="#"><i class="icon-brand-youtube"></i></a></li>
                          <li><a href="#"><i class="icon-brand-twitter"></i></a></li>
                          <li><a href="#"><i class="icon-brand-whatsapp"></i></a></li>
                          <li><a href="#"><i class="icon-brand"></i></a></li>
                      </ul>
                  </nav>
              </div>
          </div>
      </div>
  </div>

</div>

<!-- For Night mode -->
<script>
  (function (window, document, undefined) {
      'use strict';
      if (!('localStorage' in window)) return;
      var nightMode = localStorage.getItem('gmtNightMode');
      if (nightMode) {
          document.documentElement.className += ' night-mode';
      }
  })(window, document);


  (function (window, document, undefined) {

      'use strict';

      // Feature test
      if (!('localStorage' in window)) return;

      // Get our newly insert toggle
      var nightMode = document.querySelector('#night-mode');
      if (!nightMode) return;

      // When clicked, toggle night mode on or off
      nightMode.addEventListener('click', function (event) {
          event.preventDefault();
          document.documentElement.classList.toggle('night-mode');
          if (document.documentElement.classList.contains('night-mode')) {
              localStorage.setItem('gmtNightMode', true);
              return;
          }
          localStorage.removeItem('gmtNightMode');
      }, false);

  })(window, document);
</script>


<!-- javaScripts
          ================================================== -->
<script src="{{asset("js/framework.js")}}"></script>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/mmenu.min.js')}}"></script>
<script src="{{asset('js/simplebar.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

</body>

</body>


