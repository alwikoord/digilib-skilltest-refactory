@extends('landing')

@section('judul', 'Form Card')
    
@section('cons')
<div class="page-content pt-sm-0">

  <div class="page-content-inner p-sm-0"> 

  <div uk-grid>
      
      <div class="uk-width-expand@m m-4 mt-0">
          <div class="my-4">
              <h2 class="uk-text-bold">Register Digilib Card</h2>
              <hr class="m-0">
          </div>
          <div class="form">
            <form action="/form" method="post" style="text-align: center">
              @csrf
              <br>
        <div class="form-group" >
            <input type="text" class="form-control" id="" name="nama" placeholder="Masukkan Nama Anda" required>
        </div>
        @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror
  
        <div class="form-group">
            <input type="number" class="form-control" id="" name="nik" placeholder="Masukkan NIM / NIK " required>
        </div>
        @error('nik')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror
  
        <div class="form-group">
            <input type="text" class="form-control" id="" name="domisili" placeholder="Domisili" required>
        </div>
        @error('domisili')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <input type="number" class="form-control" id="" name="telepon" placeholder="Nomer Telpon" required>
        </div>
        @error('telepon')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror
  
        <div class="form-group">
            <input type="email" class="form-control" id="" name="email" placeholder="Masukkan Email Anda" required>
        </div>
        @error('email')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror
  
        <div class="form-group" id="">
             <select class="form-control" id="jk" name="jk" required>
             <option value="">Jenis Kelamin</option>
             <option>Laki-Laki</option>
             <option>Perempuan</option>
            </select>
        </div>
        @error('jk')
        <div class="alert alert-warning">{{ $message }}</div>
        @enderror        
        <button type="reset" class="btn btn-danger">Reset</button>
        <button type="submit" class="btn btn-success" name="tambah">Register</button>
        
    </form>
        </div>
      </div>

      </div>

  </div>
  
 
@endsection
  