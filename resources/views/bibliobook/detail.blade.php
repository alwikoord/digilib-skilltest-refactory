@extends('main')

@section('judul', 'Detail | Bibliobook')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Detail Biblio</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<img src="{{asset('sampul/'.$bibliobook->sampul)}}" alt=""> <br><br>
<h1>Judul : {{$bibliobook->judul}}</h1>
<h3>Penulis : {{$bibliobook->penulis}}</h3>
<h3>Penerbit : {{$bibliobook->penerbit}}</h3>
<h3>Tahun : {{$bibliobook->tahun}}</h3> <br>

<a href="/bibliobook" class="btn btn-danger" >OKE</a>

  
@endsection