@extends('main')

@section('judul', 'Create | Bibliobook')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Create Bibliobook</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')
 
<a href="/bibliobook" class="btn btn-danger btn-bg mb-3" >Back</a>
<form action="/bibliobook" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Penulis</label>
        <input type="text" name="penulis" class="form-control">
      </div>
      @error('penulis')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Penerbit</label>
        <input type="text" name="penerbit" class="form-control">
      </div>
      @error('penerbit')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Tahun</label>
        <input type="number" name="tahun" class="form-control">
      </div>
      @error('tahun')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>kategori</label>
        <select name="kategori_id" class="form-control" id="">
            <option value="">~~~ PIlih kategori ~~~</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endforeach
        </select>
      </div>
      @error('kategori_id')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Sampul</label>
        <input type="file" name="sampul" class="form-control">
      </div>
      @error('sampul')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-info">Submit</button>
  </form>

  
@endsection