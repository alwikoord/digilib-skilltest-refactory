@extends('main')

@section('judul', 'Front | Bibliobook')

@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Biblio Collection</h1>
                <a href="/bibliobook/create" class="btn btn-success btn-lg mb-2">Create Data </a> 
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>   
@endsection

@section('content')

<div class="row">
    @forelse ($bibliobook as $item)
    <div class="col-4">
        <div class="card" style="width: 30rem;">
            <img src="{{asset('sampul/'. $item->sampul)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h2 class="card-title" style="text-align: center">{{$item->judul}}</h2>
              <h3 class="card-title" style="text-align: center">Tahun : {{$item->tahun}}</h3> <br>
              <form action="/bibliobook/{{$item->id}}" method="POST" style="text-align: center">
                @csrf
                @method('DELETE')
                <a href="/bibliobook/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/bibliobook/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
            </div>
          </div>
    </div>
    @empty
        <h3>Data Biblio Kosong ....</h3>
    @endforelse
    

</div>
  
@endsection