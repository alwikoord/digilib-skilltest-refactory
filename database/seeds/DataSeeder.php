<?php

use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data')->insert([
           [ 
            'name' => 'Netflix Premium',
            'desc' => 'Layanan nonton tanpa henti dan gratis',
           ],
           [ 
            'name' => 'Canva Pro',
            'desc' => 'Layanan Editor Gratis banyak Template',
           ],
           [ 
            'name' => 'Sportify Premium',
            'desc' => 'Layanan Playlist Music Gratis',
           ]
        ]);
        //
    }
}
