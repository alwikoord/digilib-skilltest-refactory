<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberhsipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberhsip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('nama', 100);
            $table->String('nik', 200);
            $table->String('domisili', 200);
            $table->String('telepon', 50);
            $table->String('email', 100);
            $table->String('jk', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberhsip');
    }
}
